(() => {
    'use strict';

    angular
        .module('app')
        .controller('AppCtrl', MainController);

    MainController.$inject = ['AppService'];

    //Estructura de un controller
    function MainController(AppService) {
        //Variables
        var app = this;

        console.log('Hey! Im the main controller')
        
        //Star function
        //start();

        //Internal functions
        /* function getAutos() {
            AppService
                .get('api/auto', callbackFunction);

            function callbackFunction(err, data) {
                console.log(data);
            }
        } */

        /* function start() {
            getAutos();
        } */
    }
})();

//controlador de toda la apliación