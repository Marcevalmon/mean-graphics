(() => {
    'use strict';

    angular
        .module('app.autos')
        .service('AutoService', AutoService);

    AutoService.$inject = ['$http'];

    //Los servicios que ocuparas, tambien se recomienda hacer un archivo de servicio para cada cosa
    
    function AutoService($http) {

        return {
            get : get,
            post : post,
            put : put
        }

        function get(url, callback){
            $http({
                method: 'get', 
                url: url,
                headers: {}
            }).then(function (response) {
                callback(null, response.data);
            }, function(err){
                callback(err);
            });
        }

        function post(url, body, callback){
            $http({
                method : 'post',
                url : url,
                headers : {
                    'Content-type' : 'application/json; charset=utf-8'
                }, 
                data : body
            }).then(function (response) {
                callback(null, response.data);
            }, function(err){
                callback(err);
            });
        }

        function put(url, body, callback) {
            $http({
                method: 'put',
                url: url,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                },
                data: body
            }).then(function (response) {
                callback(null, response.data);
            }, function (err) {
                callback(err);
            });
        }
    }
})();