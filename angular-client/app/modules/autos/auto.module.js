(() => {
    angular
        .module('app.autos', ['ui.router'])
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    /*Me parece que estas son las configuraciones de las rutas, aqui las tienes que dar de alta
    para que puedan aparecer en el navegador y poder trabajar con ellas*/

    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
        .state({
            name: 'app.autos',
            url: '/auto',
        })
        .state({
            name: 'app.autos.list',
            url: '/list',
            templateUrl: './app/modules/autos/auto-list/auto.list.html',
            controller: 'AutoListCtrl',
            controllerAs: 'vm'
        })
        .state({
            name: 'app.autos.create',
            url: '/create',
            templateUrl: './app/modules/autos/auto-create/auto.create.html',
            controller: 'AutoCreateCtrl',
            controllerAs: 'vm'
        })
        .state({
            name: 'app.autos.delete',
            url: '/delete',
            templateUrl: './app/modules/autos/auto-delete/auto.delete.html',
            controller: 'AutoDeleteCtrl',
            controllerAs: 'vm'
        })
        .state({
            name: 'app.autos.update',
            url: '/update',
            templateUrl: './app/modules/autos/auto-update/auto.update.html',
            controller: 'AutoUpdateCtrl',
            controllerAs: 'vm'
        })
    }

})();