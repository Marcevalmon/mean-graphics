(() => {
    'use strict';

    angular
        .module('app.autos')
        .controller('AutoUpdateCtrl', AutoUpdateCtrl);

        AutoUpdateCtrl.$inject = ['AutoService'];


    //Cada servicio tiene su propio controller, que es aquí donde llegan los datos del back-end, 
    //el controller funciona para la comunicacion hacia la vista, esto sería mas el front-end    

    //Estructura de un controller
    function AutoUpdateCtrl(AutoService) {
        //Variables
        var vm = this;

        vm.cars = {
            _id : '',
            name :'',
            brands : '',
            modelo: '',
            color :'',
            anio: '', 
            estrellas:''

        }

        vm.updateCars = updateCars;

        function updateCars(cars){
            AutoService.put('/api/auto/update', cars, function(err, data){
                console.log('hola update');
                if(err) return alert(err);
                    if(data.success){
                         
                    }
            });
        }

        console.log('Hey! Im the auto-update controller')
        
       start();

       function start(){
       }
        
    }
})();

