(() => {
    'use strict';

    angular
        .module('app.autos')
        .controller('AutoListCtrl', AutoListCtrl);

        AutoListCtrl.$inject = ['AutoService'];

    //Estructura de un controller
    function AutoListCtrl(AutoService) {
        //Variables
        var vm = this;
        vm.getAutosFind = getAutosFind;

        vm.cars = {
            _id : '',
            name :'',
            brands : '',
            model: '',
            color :'',
            anio: '',
            seguridad:''

        }

        function getAutos(){
            AutoService.get('/api/auto/list', function(err, data){
                if(err) return alert(err.message);
                vm.cars = data;

                console.log('hola');
            });
        }

        function getAutosFind(name, anio, seguridad){
            console.log('entro');
            AutoService.get('/api/auto/search/' + name + '/' + anio + '/' + seguridad, function(err, data){
                if(err) return alert(err.message);
                vm.cars = data;

                console.log('hola search');
            });
        }

        console.log('Hey! Im the auto-list controller')
        
       start();

       function start(){
           getAutos();
       }
        
    }
})();

