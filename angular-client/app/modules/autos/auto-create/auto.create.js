(() => {
    'use strict';

    angular
        .module('app.autos')
        .controller('AutoCreateCtrl', AutoCreateCtrl);

        AutoCreateCtrl.$inject = ['AutoService'];

    //Estructura de un controller
    function AutoCreateCtrl(AutoService) {
        //Variables
        var vm = this;
        vm.cars = {
            _id : '',
            name :'',
            brands : '',
            modelo: '',
            color :'',
            anio: '',
            seguridad:'',
        }

        vm.addCars = addCars;

        function addCars(cars){
            AutoService.post('/api/auto/create', cars, function(err, data){
                console.log('hola');
                if(err) return alert(err);
                    if(data.success){
                        getAutos(); 
                    }
            });
        }

        console.log('Hey! Im the auto-create controller')
        
       start();

       function start(){
       }
        
    }
})();

