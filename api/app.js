var express = require('express');
var app = express();
var mongoose = require('mongoose');
var cors = require('cors');
var bodyParser = require('body-parser');
var helmet = require('helmet');

//Conexión a la base de datos, este es el archivo para inicializar la aplicación
//comando: node app.js
//Antes tienes que tener levantado mongo
//Si quieres ver como funciona la apliación, necesitas restaurar la base
//Carpeta api, va el back-end
//Carpeta angular-client, va todo el front-end
//Archivos app.ctrl, app.module, app.service, archivos de la aplicacion
/*ARCHIVOS A REVISAR:
		1.- api/routes/auto.route.js
		2.- api/models/etc <--Modelos de los json-->
		3.- package.json <--Dependencias instaladas-->
		4.- angular-client/app.services.js
		5.- angular-client/app/modules/autos/auto.module - auto.service
		6.- angular-client/app/modules/autos/auto-create - auto/list - etc
*/

//PARA VISUALIZAR EL LISTADO, CREACION ,ETC DE UN AUTO, PON LA URL xd 
//ESO ME FALTO, HACER LAS REDIRECCIONES jaja 
//EJEMPLO: http://localhost:3000/#!/app/auto/list solo cambia lo ultimo por create, delete y search

app.use(express.static(__dirname + '../../angular-client'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());
app.use(helmet());

mongoose.connect('mongodb://localhost/auto_db', function (err) {
    if (err) return console.log('Error al conectarse a la base', err);
    console.log('Conexion a la base exitosa');
});



require('./routes/auto.route')(app);

app.listen(3000, function () {
    console.log('Servidor corriendo en http://localhost:3000' );
});