
module.exports = function (app) {

 
    var autoModel = require('../models/auto.model');
    var brandModel = require('../models/brands.model');

    /*TODO ESTO ES EL BACK-END, donde se traen los datos de la base de datos*/

    //Rutas
    //==========================================================================
    
    app.get('/api/auto/list', getAuto);
    app.get('/api/auto/search/:name/:anio/:seguridad', findFolio);
    app.post('/api/auto/create',  createAuto);
    app.post('/api/auto/delete', borrarAuto);
    app.put('/api/auto/update', actualizarAuto);
 
    //Funciones
    //==========================================================================

    function getAuto(req, res) {
         //{} seria para traer todos, sin ningun filtro
         autoModel.find({}, function(err, cars){
            if(err) return res.status(500).send({err : err});
            res.send(cars); //envio de todas las tareas
        });
    }

    function createAuto(req, res) {
        var auto = new autoModel(req.body);
        auto.model="POST";
        auto.save(auto, function(err, auto){
            if(err) console.log(err.message);
            console.log(auto);
            res.send(auto);

        });
       
    }

    function borrarAuto(req, res) {
        console.log('entra',req.body);
        autoModel.remove({_id: req.body._id},
            function (err){
                if(err) return res.status(500).send({err : err});
                res.send({success:true})
            })
    }

    function findFolio(req, res){
        var query = {};
        if(req.params.name !== 'undefined') query.name = req.params.name;
        if(req.params.anio !== 'undefined') query.anio = req.params.anio;
        if(req.params.seguridad !== 'undefined') query.seguridad = req.params.seguridad;
        autoModel.find(query,function(err, cars){
            if(err) return res.status(500).send({err:err});
            res.send(cars);
        });
    }

    /* function actualizarAuto(req, res){
        var auto = new autoModel();
        auto._id = parseInt(req.body._id);
        console.log('entre');
        autoModel.find({_id: auto._id},
        function (err) {
            if(err) return res.status(500).send({err:err});
            res.send({success:true})
        })
    } */

    function actualizarAuto(req, res){
        var id = req.body._id;
        delete req.body._id;
        console.log('entre',req.body);
        autoModel.update({_id: id},{
            $set:req.body
        },
        function (err) {
            console.log(err)
            if(err) return res.status(500).send({err:err});
            res.send({success:true})
        })
    }
   
    function brand(req, res){
        brandModel.find({}, function(err, cars){
            if(err) return res.status(500).send({err : err});
            res.send(cars); //envio de todas las tareas
        });
    }
};