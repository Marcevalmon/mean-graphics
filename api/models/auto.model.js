var mongoose = require('mongoose');

var autoModel = new mongoose.Schema({
    _id: { type: Number },
    name:{type:String},
    brands: {type:Number},
    modelo: {type:String},
    anio: { type: Number },
    color : { type: String },
    seguridad : {type:Number}
    //imagen : { type: File }
});

module.exports = mongoose.model('auto', autoModel);