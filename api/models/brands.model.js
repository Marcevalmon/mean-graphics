var mongoose = require('mongoose');

var brandModel = new mongoose.Schema({
    _id: { type: Number },
    name : { type: String }
    //color : { type: String },
    //imagen : { type: File }
});

module.exports = mongoose.model('brands', brandModel);